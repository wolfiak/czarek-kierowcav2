import { Injectable } from '@angular/core';
import LazyTarget from '../interfaces/LazyTarget';

@Injectable()
export class LazyViewportService {
  private observer: IntersectionObserver;
  private targets: Map<Element, LazyTarget>;

  constructor() {
    this.observer = null;
    this.targets = new Map();
  }

  public addTarget(target: LazyTarget): void {
    if (this.observer) {
      this.targets.set(target.element, target);
      this.observer.observe(target.element);
    } else {
      target.updateVisibility(true, 1.0);
    }
  }

  public setup(): void {
    if (!('IntersectionObserver' in window)) {
      return;
    }
    this.observer = new IntersectionObserver(this.handleIntersectionUpadte, {
      rootMargin: `0px`,
      threshold: 0.1
    });
  }

  public removeTarget(target: LazyTarget): void {
    if (this.observer) {
      this.targets.delete(target.element);
      this.observer.unobserve(target.element);
    }
  }

  private handleIntersectionUpadte = (
    enteries: IntersectionObserverEntry[]
  ): void => {
    for (const entry of enteries) {
      const lazyTarget = this.targets.get(entry.target);
      if (lazyTarget) {
        lazyTarget.updateVisibility(
          entry.isIntersecting,
          entry.intersectionRatio
        );
      }
    }
  }
}
