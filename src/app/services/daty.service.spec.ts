import { TestBed, inject } from '@angular/core/testing';

import { DatyService } from './daty.service';

describe('DatyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatyService]
    });
  });

  it('should be created', inject([DatyService], (service: DatyService) => {
    expect(service).toBeTruthy();
  }));
});
