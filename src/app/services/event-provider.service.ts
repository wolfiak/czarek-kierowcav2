import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EventProviderService {
  lista: Map<string, Subject<any>> = new Map();
  constructor() {}

  addEvento(ktory: string, callback: Subject<any>) {
    console.log('Dodanie', ktory);
    this.lista.set(ktory, callback);
  }

  get(ktory: string) {
    console.log('Pobranie wartosci', ktory);
    const callb = this.lista.get(ktory);
    this.lista.delete(ktory);
    return callb;
  }
}
