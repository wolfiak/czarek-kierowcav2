import { TestBed, inject } from '@angular/core/testing';

import { LazyViewportService } from './lazy-viewport.service';

describe('LazyViewportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LazyViewportService]
    });
  });

  it('should be created', inject([LazyViewportService], (service: LazyViewportService) => {
    expect(service).toBeTruthy();
  }));
});
