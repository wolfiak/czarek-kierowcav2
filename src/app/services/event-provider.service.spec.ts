import { TestBed, inject } from '@angular/core/testing';

import { EventProviderService } from './event-provider.service';

describe('EventProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventProviderService]
    });
  });

  it('should be created', inject([EventProviderService], (service: EventProviderService) => {
    expect(service).toBeTruthy();
  }));
});
