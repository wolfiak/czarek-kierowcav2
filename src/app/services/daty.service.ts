import { Injectable } from '@angular/core';

@Injectable()
export class DatyService {
  private rok: number;
  private miesiac: number;
  constructor() {}

  getMiesiac(): string {
    switch (new Date().getMonth()) {
      case 0: {
        return 'Styczeń';
      }
      case 1: {
        return 'Luty';
      }
      case 2: {
        return 'Marzec';
      }
      case 3: {
        return 'Kwiecień';
      }
      case 4: {
        return 'Maj';
      }
      case 5: {
        return 'Czerwiec';
      }
      case 6: {
        return 'Lipiec';
      }
      default: {
        return 'Błąd !';
      }
    }
  }
  getLiczbaDni(
    rok: number = new Date().getFullYear(),
    miesaic: number = new Date().getMonth()
  ): any[] {
    this.rok = rok;
    this.miesiac = miesaic;
    return new Array(new Date(rok, miesaic, 0).getDate())
      .fill(0)
      .map((elm, index) => {
        return { dzien: index + 1, zajety: false };
      });
  }
  setMiesiac(liczba) {
    const data = new Date();
    if (liczba === 1) {
      // if(miesiac > 11){
      // }
    }
  }
  getDzis(): number {
    return new Date().getDate();
  }
}
