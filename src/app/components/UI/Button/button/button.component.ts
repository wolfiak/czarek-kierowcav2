import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { EventProviderService } from '../../../../services/event-provider.service';

@Component({
  selector: 'czarek-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnDestroy {
  @Input() nazwa: string;
  @Output() klik: EventEmitter<any> = new EventEmitter();
  @Input() niedostepny = false;
  @Input() klasy: string[];
  @Input() kanaly: string[];
  @Input() niedostepnyTekst: string;

  constructor(private elm: ElementRef, private evento: EventProviderService) {}

  ngOnInit() {
    if (this.kanaly) {
      this.kanaly.forEach(elm => {
        this.evento.get(elm).subscribe(res => {
          this.stanPrzycisku(`Dodaj swój termin, ${res}`, true);
        });
      });
    }

    if (this.niedostepny) {
      this.stanPrzycisku(this.niedostepnyTekst, this.niedostepny);
    }
  }
  ngOnDestroy() {}
  getKlasy(): string {
    return this.klasy.join(' ');
  }
  onClick() {
    this.klik.emit(null);
  }
  private stanPrzycisku(tekst: string, stan: boolean): void {
    this.nazwa = tekst;
    this.elm.nativeElement.disabled = stan;
  }
}
