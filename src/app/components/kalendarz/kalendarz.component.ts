import { Component, OnInit } from '@angular/core';
import { DatyService } from '../../services/daty.service';
import { Subject } from 'rxjs/Subject';
import { EventProviderService } from '../../services/event-provider.service';

@Component({
  selector: 'czarek-kalendarz',
  templateUrl: './kalendarz.component.html',
  styleUrls: ['./kalendarz.component.scss']
})
export class KalendarzComponent implements OnInit {
  public liczbaDni: any[];
  public miesiac: string;
  public odblokujPrzycisk: Subject<string> = new Subject();
  constructor(
    private daty: DatyService,
    private evento: EventProviderService
  ) {}

  ngOnInit() {
    this.evento.addEvento('odblokujPrzycisk', this.odblokujPrzycisk);
    this.liczbaDni = this.daty.getLiczbaDni();
    this.miesiac = this.daty.getMiesiac();
  }
  onClick(dzien) {
    this.odblokujPrzycisk.next(`${dzien} ${this.miesiac}`);
  }
  nastepnyMiesiac() {
    console.log('Nastepny');
  }
  poprzedniMiesiac() {
    console.log('Poprzedni');
  }
}
