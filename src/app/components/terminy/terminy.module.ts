import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TerminyRoutingModule } from './terminy-routing.module';
import { TerminyComponent } from './terminy/terminy.component';
import { KalendarzComponent } from '../kalendarz/kalendarz.component';
import { DatyService } from '../../services/daty.service';
import { DataDirective } from '../../directives/data.directive';
import { ButtonComponent } from '../UI/Button/button/button.component';
import { EventProviderService } from '../../services/event-provider.service';

@NgModule({
  imports: [CommonModule, TerminyRoutingModule],
  declarations: [
    TerminyComponent,
    KalendarzComponent,
    DataDirective,
    ButtonComponent
  ],
  providers: [DatyService, EventProviderService]
})
export class TerminyModule {}
