import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TerminyComponent } from './terminy/terminy.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: TerminyComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TerminyRoutingModule { }
