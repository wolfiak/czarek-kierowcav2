import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminyComponent } from './terminy.component';

describe('TerminyComponent', () => {
  let component: TerminyComponent;
  let fixture: ComponentFixture<TerminyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
