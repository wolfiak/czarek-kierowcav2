import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PathNotFoundComponent } from './components/path-not-found/path-not-found.component';
import { NaviagationComponent } from './components/naviagation/naviagation.component';
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { LazyViewportService } from './services/lazy-viewport.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PathNotFoundComponent,
    NaviagationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'czarek'}),
    AppRoutingModule,
    SharedModule,
    environment.production
      ? ServiceWorkerModule.register('/ngsw-worker.js')
      : [],
      HttpClientModule
  ],
  providers: [LazyViewportService],
  bootstrap: [AppComponent]
})
export class AppModule {}
