import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazySrcDirective } from '../directives/lazy-src.directive';


@NgModule({
  imports: [CommonModule],
  declarations: [LazySrcDirective ],
  exports: [LazySrcDirective ]
})
export class SharedModule {}
