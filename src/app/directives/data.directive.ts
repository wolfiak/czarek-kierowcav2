import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { DatyService } from '../services/daty.service';

@Directive({
  selector: '[czarekData]'
})
export class DataDirective implements OnInit {
  @Input() dzis: number;
  constructor(private elm: ElementRef, private daty: DatyService) {}

  ngOnInit() {
    if (this.dzis === this.daty.getDzis()) {
      this.elm.nativeElement.style.border = ' 1px solid var(--var-szary)';
      this.elm.nativeElement.style.borderRadius = '21%';
    }
  }
}
