import {
  Directive,
  Input,
  ElementRef,
  Renderer2,
  OnInit,
  OnDestroy
} from '@angular/core';
import { LazyViewportService } from '../services/lazy-viewport.service';
import LazyTarget from '../interfaces/LazyTarget';

@Directive({
  selector: '[czarekLazySrc]'
})
export class LazySrcDirective implements OnInit, OnDestroy, LazyTarget {
  @Input() lazySrc: string;
  @Input() visibleClass: string;
  public element: Element;

  constructor(
    private elementRef: ElementRef,
    private lazyViewport: LazyViewportService,
    private renderer: Renderer2
  ) {
    this.element = elementRef.nativeElement;
  }

  public ngOnDestroy(): void {
    if (this.lazyViewport) {
      this.lazyViewport.removeTarget(this);
    }
  }

  public ngOnInit(): void {
    this.lazyViewport.addTarget(this);
  }
  public updateVisibility(isVisable: boolean, ratio: number): void {
    if (!isVisable) {
      return;
    }
    this.lazyViewport.removeTarget(this);
    this.lazyViewport = null;
    this.renderer.setProperty(this.element, 'src', this.lazySrc);

    if (this.visibleClass) {
      this.renderer.addClass(this.element, this.visibleClass);
    }
  }
}
