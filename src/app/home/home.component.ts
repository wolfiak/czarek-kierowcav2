import { Component, OnInit, ElementRef } from '@angular/core';
import { LazyViewportService } from '../services/lazy-viewport.service';

@Component({
  selector: 'czarek-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  duzy: boolean;
  constructor(private lazy: LazyViewportService, private elm: ElementRef) {}

  ngOnInit() {
    if (window.screen.width <= 600) {
      this.duzy = false;
    } else {
      this.duzy = true;
    }
    this.lazy.setup();
  }
  scroll(target) {
    target.scrollIntoView({ behavior: 'smooth' });
  }
}
