import { CzarekKierowcaV2Page } from './app.po';

describe('czarek-kierowca-v2 App', () => {
  let page: CzarekKierowcaV2Page;

  beforeEach(() => {
    page = new CzarekKierowcaV2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to czarek!');
  });
});
