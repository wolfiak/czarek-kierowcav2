import { browser, by, element } from 'protractor';

export class CzarekKierowcaV2Page {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('czarek-root h1')).getText();
  }
}
